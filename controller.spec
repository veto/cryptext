# -*- mode: python -*-
a = Analysis([os.path.join(HOMEPATH,'support\\_mountzlib.py'), os.path.join(HOMEPATH,'support\\useUnicode.py'), 'controller.py'],
             pathex=['Z:\\controller'])
pyz = PYZ(a.pure)
exe = EXE(pyz,
          a.scripts,
          exclude_binaries=1,
          name=os.path.join('build\\pyi.win32\\controller', 'controller.exe'),
          debug=False,
          strip=False,
          upx=True)
coll = COLLECT( exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               name=os.path.join('dist', 'controller'))
app = BUNDLE(coll,
             name=os.path.join('dist', 'controller.app'))
